package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.ListOfLinksModel;
import com.gsk.aem_platform.web_factory.core.objects.MenuItem;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

/**
 * Sling Model Implementation of List Of Links Model
 *
 * @author Photon
 *
 */
@Model(adaptables = Resource.class, adapters = ListOfLinksModel.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListOfLinksModelImpl implements ListOfLinksModel {

	Logger logger = LoggerFactory.getLogger(ListOfLinksModelImpl.class);

	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@Inject
	private String sectionHeading;
	
	@Inject
	@Default(values = "{}")
	private String[] listofLinks;

	private List<MenuItem> listItems;
	
	@PostConstruct
	protected void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("listOfLinks", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
				
		Gson gson = new Gson();
		
		listItems = new LinkedList<MenuItem>();
		
		for (String linkString : listofLinks) {
			MenuItem menuItem = gson.fromJson(linkString, MenuItem.class);
			listItems.add(menuItem);
		}
	}
	
	@Override
	public String getSectionHeading() {
		return sectionHeading;
	}
	
	@Override
	public List<MenuItem> getListItems() {
		return listItems;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}