package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.Teaser;

/**
 * List of Teasers Model
 *
 * @author Photon
 *
 */
public interface ListOfTeasersModel extends BaseComponentModel {
	
	public String getSectionHeading();
	
	public String getViewType();
	
	public List<Teaser> getTeaserItems();

}
