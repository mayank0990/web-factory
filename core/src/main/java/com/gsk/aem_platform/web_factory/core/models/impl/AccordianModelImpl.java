package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.AccordianModel;
import com.gsk.aem_platform.web_factory.core.objects.AccordianItem;

/**
 * Sling Model Implementation of Accordian Model
 *
 * @author Photon
 *
 */
@Model(adaptables=Resource.class,adapters=AccordianModel.class)
public class AccordianModelImpl implements AccordianModel { 
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(AccordianModelImpl.class);
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	@Inject @Optional
	private String sectionHeading;
	
	@Inject @Optional
	private String displayMode;
	
	@Inject @Optional
	@Default(values="false")
	private String allowMultipleOpenItems;
	
	@Inject @Optional
	@Default(values="{}")
	private String[] accordianItems;
	
	private List<AccordianItem> accordianItemList;
	
	
	@PostConstruct
	public void init() {
		
		Gson gson = new Gson();
		accordianItemList = new LinkedList<AccordianItem>();
		int count=0;
		for(String itemString : accordianItems) {
			AccordianItem accordianItem = gson.fromJson(itemString, AccordianItem.class);
			if(accordianItem != null) {
				count++;
				accordianItem.setParsysId("accordianPar"+count);
				accordianItemList.add(accordianItem);
			}
		}	
	}
	
	@Override
	public String getSectionHeading() {
		return sectionHeading;
	}
	
	@Override
	public String getDisplayMode() {
		return displayMode;
	}
	
	@Override
	public String getAllowMultipleOpenItems() {
		return allowMultipleOpenItems;
	}
	
	@Override
	public List<AccordianItem> getAccordianItemList() {
		return accordianItemList;
	}
	
}