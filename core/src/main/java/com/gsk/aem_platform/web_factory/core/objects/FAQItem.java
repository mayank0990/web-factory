package com.gsk.aem_platform.web_factory.core.objects;

import java.util.List;

/**
 * FAQItem DTO
 *
 * @author Photon
 *
 */
public class FAQItem {

	private String categoryTitle;
	private String categoryBodyTitle;
	private List<AccordianItem> accordianItems;
	
	public String getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}
	public String getCategoryBodyTitle() {
		return categoryBodyTitle;
	}
	public void setCategoryBodyTitle(String categoryBodyTitle) {
		this.categoryBodyTitle = categoryBodyTitle;
	}
	public List<AccordianItem> getAccordianItems() {
		return accordianItems;
	}
	public void setAccordianItems(List<AccordianItem> accordianItems) {
		this.accordianItems = accordianItems;
	}
	
}