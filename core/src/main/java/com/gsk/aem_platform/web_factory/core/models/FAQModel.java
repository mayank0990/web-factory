package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.FAQItem;

/**
 * FAQ Model
 *
 * @author Photon
 *
 */
public interface FAQModel extends BaseComponentModel {
	
	public String getSectionHeading();
	
	public String getSubHeading();
	
	public String getMobileSelectText();
	
	public List<FAQItem> getCategoryList();
	
}
