package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.PageObject;

/**
 * Page Listing Model
 *
 * @author Photon
 *
 */
public interface PageListingModel extends BaseComponentModel {
	
	public List<PageObject> getPages();
	
	public String getHeading();
	
	public String getCtaLabel();
	
	public String getLimitResult();

	public int getMaxResultLimit();
	
	public String getLoadMoreText();
	
	public String getLoadMoreLink();
	
}
