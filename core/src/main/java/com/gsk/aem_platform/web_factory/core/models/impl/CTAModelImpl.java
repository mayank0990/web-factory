package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.CTAModel;
import com.gsk.aem_platform.web_factory.core.objects.CTA;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

@Model(adaptables=Resource.class,adapters=CTAModel.class)
public class CTAModelImpl implements CTAModel { 
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(CTAModelImpl.class);
	
	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@Inject @Optional
	private String align;
	
	@Inject @Optional
	@Default(values="{}")
	private String[] ctaItems;
	
	private List<CTA> ctaList;
	
	@PostConstruct
	public void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("cta", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
		Gson gson = new Gson();
		ctaList = new LinkedList<CTA>();
		
		for(String itemString : ctaItems) {
			CTA ctaItem = gson.fromJson(itemString, CTA.class);
			if(ctaItem != null) {
				ctaList.add(ctaItem);
			}
		}
	}
	
	@Override
	public String getAlign() {
		return align;
	}
	
	@Override
	public List<CTA> getCtaList() {
		return ctaList;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}