package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.MenuItem;

/**
 * List Of Links Model
 *
 * @author Photon
 *
 */

public interface ListOfLinksModel extends BaseComponentModel {
	
	public String getSectionHeading();
	
	public List<MenuItem> getListItems();

}
