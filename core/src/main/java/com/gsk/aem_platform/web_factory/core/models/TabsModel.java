package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.AccordianItem;

/**
 * Tabs Model
 *
 * @author Photon
 *
 */

public interface TabsModel extends BaseComponentModel {
	
	public String getSectionHeading();
	
	public List<AccordianItem> getTabItemList();
	
}
