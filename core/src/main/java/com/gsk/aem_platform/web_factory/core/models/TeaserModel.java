package com.gsk.aem_platform.web_factory.core.models;

/**
 * Teaser Model
 *
 * @author Photon
 *
 */

public interface TeaserModel extends BaseComponentModel {
	
	public String getTitle();
	
	public String getImagePath();
	
	public String getImageAltText();
	
	public String getDescription();
	
	public String getCtaText();
	
	public String getCtaOpenInNewWindow();
	
	public String getLink();
	
}
