package com.gsk.aem_platform.web_factory.core.models;

import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;

/**
 * Base Component Model
 *
 * @author Photon
 *
 */
public interface BaseComponentModel {
	
	public WCMComponent getComponent();
	
}
