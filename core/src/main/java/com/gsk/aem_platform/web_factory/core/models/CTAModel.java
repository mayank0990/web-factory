package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.CTA;

/**
 * CTA Model
 *
 * @author Photon
 *
 */
public interface CTAModel extends BaseComponentModel {
	
	public String getAlign();
	
	public List<CTA> getCtaList();
	
}
