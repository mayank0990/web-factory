package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.PageListingModel;
import com.gsk.aem_platform.web_factory.core.objects.PageObject;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;
import com.gsk.aem_platform.web_factory.core.util.LinkUtil;

/**
 * Sling Model Implementation of Page Listing Model
 *
 * @author Photon
 *
 */
@Model(adaptables = Resource.class, adapters = PageListingModel.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PageListingModelImpl implements PageListingModel {
	
	Logger log = LoggerFactory.getLogger(PageListingModelImpl.class);

	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@Inject @Optional
	private ResourceResolver resourceResolver;

	@Inject @Optional
	private String heading;

	@Inject @Optional
	private String itemsCommonCtaLabel;
	
	@Inject @Optional
	@Default(values="fixed")
	private String listingType;
	
	@Inject @Optional
	@Default(values= "{}")
	private String[] fixedPages;
	
	@Inject @Optional
	private String parentPagePath;
	
	@Inject @Optional
	@Default(values = "hierarchy")
	private String orderBy;
	
	@Inject @Optional
	@Default(values = "false")
	private String limitResult;
	
	@Inject @Optional
	@Default(intValues = 9)
	private int maxResultLimit;
	
	@Inject @Optional
	private String loadMoreText;
	
	@Inject @Optional
	private String loadMoreLink;
	
	private List<PageObject> pages;
	
	@PostConstruct
	protected void init() {
		
		try {
		
			wcmComponent = componentUtilService.getComponentDetails("page-listing", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
			
			if(StringUtils.isNotEmpty(listingType)) {
				if(listingType.equalsIgnoreCase("fixed")) {
					pages = new LinkedList<PageObject>();
					Gson gson = new Gson();
					for(String itemString : fixedPages) {
						PageObject pageObject = gson.fromJson(itemString, PageObject.class);
						Resource pageResource = resourceResolver.getResource(pageObject.getPagePath());
						if((pageResource != null) && (pageResource instanceof Resource)) {
							Page page = pageResource.adaptTo(Page.class);
							if(page != null) {
								ValueMap pageProperties = page.getProperties();
								pageObject.setTitle(pageProperties.get("jcr:title",pageProperties.get("title",String.class)));
								pageObject.setDescription(pageProperties.get("jcr:description",pageProperties.get("description",String.class)));
								pageObject.setSummary(pageProperties.get("summary",""));
								pageObject.setThumbnailImagePath(pageProperties.get("thumbnailImagePath",pageProperties.get("imagePath",String.class)));
								pageObject.setThumbnailImageAltText(pageProperties.get("thumbnailImageAltText",pageProperties.get("imageAltText",String.class)));
								pages.add(pageObject);
							}
						}
					}
				} else if(listingType.equalsIgnoreCase("child-pages")) {
					pages = new ArrayList<PageObject>();
					Resource parentResource = resourceResolver.getResource(parentPagePath);
					if((parentResource != null) && (parentResource instanceof Resource)) {
						Page parentPage = parentResource.adaptTo(Page.class);
						if(parentPage != null) {
							Iterable<Resource> childResources = parentResource.getChildren();
							for(Resource childResource : childResources) {
								if(!childResource.getName().equalsIgnoreCase("jcr:content")) {
									Page childPage = childResource.adaptTo(Page.class);
									if(childPage != null) {
										ValueMap pageProperties = childPage.getProperties();
										PageObject pageObject = new PageObject();
										pageObject.setTitle(pageProperties.get("title",pageProperties.get("jcr:title",String.class)));
										pageObject.setDescription(pageProperties.get("summary",pageProperties.get("jcr:description",String.class)));
										pageObject.setImagePath(pageProperties.get("thumbnailImage",pageProperties.get("imagePath",String.class)));
										pageObject.setImagePath(pageProperties.get("thumbnailImageAltText",pageProperties.get("imageAltText",String.class)));
										pages.add(pageObject);
									}
								}
							}
						}
					}
				}
			} 
		} catch(Exception e) {
			log.error("Exception in init method of PageListingModelImpl :: " + e.getMessage()); 
		}
	}
	
	@Override
	public List<PageObject> getPages() {
		return pages;
	}

	@Override
	public String getHeading() {
		return heading;
	}
	
	@Override
	public String getLimitResult() {
		return limitResult;
	}

	@Override
	public int getMaxResultLimit() {
		return maxResultLimit;
	}
	
	@Override
	public String getLoadMoreText() {
		return loadMoreText;
	}
	
	@Override
	public String getLoadMoreLink() {
		return LinkUtil.getFormattedURL(loadMoreLink);
	}

	@Override
	public String getCtaLabel() {
		return itemsCommonCtaLabel;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}