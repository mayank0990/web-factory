package com.gsk.aem_platform.web_factory.core.services;

import java.util.Map;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;

public interface ComponentsUtilService {

	public Map<String,WCMComponent> getComponents();
	
	public WCMComponent getComponentDetails(String componentName, String customHeight, String customStyleClass, String hideOnMobile, String hideOnTablet);
	
}
