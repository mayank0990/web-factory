package com.gsk.aem_platform.web_factory.core.models.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gsk.aem_platform.web_factory.core.models.SocialLinksModel;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

/**
 * Sling Model Implementation of Social Links Model
 *
 * @author Photon
 *
 */
@Model(adaptables=Resource.class, adapters=SocialLinksModel.class)
public class SocialLinksModelImpl implements SocialLinksModel {

	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(SocialLinksModel.class);
	
	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@PostConstruct
	public void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("socialLinks", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
}
