package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.FAQModel;
import com.gsk.aem_platform.web_factory.core.objects.FAQItem;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

/**
 * Sling Model Implementation of FAQ Model
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Model(adaptables=Resource.class,adapters=FAQModel.class)
public class FAQModelImpl implements FAQModel { 
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(FAQModelImpl.class);
	
	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@Inject @Optional
	private String sectionHeading;
	
	@Inject @Optional
	private String subHeading;
	
	@Inject @Optional
	private String mobileSelectText;
	
	@Inject @Optional
	@Default(values="{}")
	private String[] categories;
	
	private List<FAQItem> categoryList;
	
	
	@PostConstruct
	public void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("faq", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
		Gson gson = new Gson();
		categoryList = new LinkedList<FAQItem>();
		
		for(String itemString : categories) {
			FAQItem faqItem = gson.fromJson(itemString, FAQItem.class);
			if(faqItem != null) {
				categoryList.add(faqItem);
			}
		}
	}
	
	@Override
	public String getSectionHeading() {
		return sectionHeading;
	}
	
	@Override
	public String getSubHeading() {
		return subHeading;
	}
	
	@Override
	public String getMobileSelectText() {
		return mobileSelectText;
	}
	
	@Override
	public List<FAQItem> getCategoryList() {
		return categoryList;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}