package com.gsk.aem_platform.web_factory.core.models.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gsk.aem_platform.web_factory.core.models.TeaserModel;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;
import com.gsk.aem_platform.web_factory.core.util.LinkUtil;

/**
 * Sling Model Implementation of Teaser Model
 *
 * @author Photon
 *
 */
@Model(adaptables=Resource.class,adapters=TeaserModel.class)
public class TeaserModelImpl implements TeaserModel { 
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(TeaserModelImpl.class);
	
	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	WCMComponent wcmComponent;
		
	@Inject @Optional
	private String imagePath;
	
	@Inject @Optional
	private String imageAltText;
	
	@Inject @Optional
	private String title;
	
	@Inject @Optional
	private String description;
	
	@Inject @Optional
	private String ctaText;
	
	@Inject @Optional
	private String ctaOpenInNewWindow;
	
	@Inject @Optional
	private String link;
	
	@PostConstruct
	public void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("teaser", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public String getImagePath() {
		return imagePath;
	}
	
	@Override
	public String getImageAltText() {
		return imageAltText;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public String getCtaText() {
		return ctaText;
	}
	
	@Override
	public String getCtaOpenInNewWindow() {
		return ctaOpenInNewWindow;
	}
	
	@Override
	public String getLink() {
		return LinkUtil.getFormattedURL(link);
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}