package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.AccordianItem;

/**
 * Accordian Model
 *
 * @author Photon
 *
 */
public interface AccordianModel {
	
	public String getSectionHeading();
	
	public String getDisplayMode();
	
	public String getAllowMultipleOpenItems();
	
	public List<AccordianItem> getAccordianItemList();
	
}
