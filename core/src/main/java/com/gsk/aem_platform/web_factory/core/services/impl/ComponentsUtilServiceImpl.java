package com.gsk.aem_platform.web_factory.core.services.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Implementation of Components Configuration Service
 * 
 * @author Photon
 */
@Component(metatype = true, immediate = true, label = "Components Configuration Service")
@Service(ComponentsUtilService.class)
@Properties({
    @Property(name = "service.pid", value = "com.gsk.aem_platform.web_factory.core.services.impl.ComponentsConfigurationServiceImpl", propertyPrivate = false),
    @Property(name = "service.vendor", value = "GSK", propertyPrivate = false) })
public class ComponentsUtilServiceImpl implements ComponentsUtilService {

    private static final Logger log = LoggerFactory.getLogger(ComponentsUtilServiceImpl.class);
    
    @Activate
    protected final void activate(final Map<Object, Object> config) {
        log.info("Activated ComponentsConfigurationServiceImpl");
    }

    @Deactivate
    protected void deactivate(ComponentContext ctx) throws Exception {
        log.info("Deactivated ComponentsConfigurationServiceImpl");
    }
    
    @Override
    public Map<String,WCMComponent> getComponents(){
    	
    	Map<String,WCMComponent> components = new HashMap<String,WCMComponent>();
    	
		return components;
    }
    
    @Override 
    public WCMComponent getComponentDetails(String componentName, String customHeight, String customStyleClass, String hideOnMobile, String hideOnTablet) {
		
		WCMComponent componentDetails = new WCMComponent();
		
		Random random = new Random();
		int randomNumber = random.nextInt(10000);
		
		componentDetails.setName(componentName);
		componentDetails.setId(componentName + randomNumber);
		componentDetails.setCustomHeight(customHeight);
		componentDetails.setCustomStyleClass(customStyleClass);
		componentDetails.setHideOnMobile(hideOnMobile);
		componentDetails.setHideOnTablet(hideOnTablet);
		componentDetails.setRandomNumber(Integer.toString(randomNumber));
		
		return componentDetails;	
	}
    
}