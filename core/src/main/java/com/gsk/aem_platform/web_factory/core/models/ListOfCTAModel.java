package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.objects.CTA;

/**
 * List of CTA Model
 *
 * @author Photon
 *
 */
public interface ListOfCTAModel extends BaseComponentModel {

	public String getAlignment();

	public List<CTA> getCTAMap();

}
