package com.gsk.aem_platform.web_factory.core.objects;

import java.util.List;
import com.gsk.aem_platform.web_factory.core.util.LinkUtil;

/**
 * MenuItem DTO
 *
 * @author Photon
 *
 */
public class MenuItem {
	
	private String text;
	private String url;
	private String imagePath;
	private String imageAltText;
	private String tag;
	private String iconType;
	private String iconClass;
	private String isCurrent;
	private String mode;
	private String parentPagePath;
	private String flyoutPagePath;
	private String subFlyoutPagePath;
	private List<MenuItem> subLinks;
	private String openInNewWindow;
	private String filterText;
	private String filterTag;
	
	public String getFilterTag() {
		return filterTag;
	}
	public void setFilterTag(String filterTag) {
		this.filterTag = filterTag;
	}
	public String getFilterText() {
		return filterText;
	}
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUrl() {
		return LinkUtil.getFormattedURL(url);
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getImageAltText() {
		return imageAltText;
	}
	public void setImageAltText(String imageAltText) {
		this.imageAltText = imageAltText;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}
	public String getIconType() {
		return iconType;
	}
	public void setIconType(String iconType) {
		this.iconType = iconType;
	}
	public String getIsCurrent() {
		return isCurrent;
	}
	public void setIsCurrent(String isCurrent) {
		this.isCurrent = isCurrent;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getParentPagePath() {
		return parentPagePath;
	}
	public void setParentPagePath(String parentPagePath) {
		this.parentPagePath = parentPagePath;
	}
	public String getFlyoutPagePath() {
		return flyoutPagePath;
	}
	public void setFlyoutPagePath(String flyoutPagePath) {
		this.flyoutPagePath = flyoutPagePath;
	}
	public String getSubFlyoutPagePath() {
		return subFlyoutPagePath;
	}
	public void setSubFlyoutPagePath(String subFlyoutPagePath) {
		this.subFlyoutPagePath = subFlyoutPagePath;
	}
	public List<MenuItem> getSubLinks() {
		return subLinks;
	}
	public void setSubLinks(List<MenuItem> subLinks) {
		this.subLinks = subLinks;
	}
	public String getOpenInNewWindow() {
		return openInNewWindow;
	}
	public void setOpenInNewWindow(String openInNewWindow) {
		this.openInNewWindow = openInNewWindow;
	}
	
}