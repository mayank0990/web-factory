package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gsk.aem_platform.web_factory.core.models.ColumnGridModel;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

/**
 * Sling Model Implementation of Column Grid Model
 *
 * @author Photon
 *
 */
@Model(adaptables=Resource.class,adapters=ColumnGridModel.class)
public class ColumnGridModelImpl implements ColumnGridModel { 
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(ColumnGridModelImpl.class);
	
	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight;
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	@Inject @Optional
	private String sectionHeading;
	
	private WCMComponent wcmComponent;
	
	@Inject @Optional
	@Default(intValues=1)
	private int columns;
	
	private List<String> columnList;
	
	@PostConstruct
	public void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("columnGrid", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
		columnList = new ArrayList<String>();
		
		for(int count=0; count<columns; count++) {
			columnList.add("col-par-"+count+1);
		}
	}
	
	@Override
	public String getSectionHeading() {
		return sectionHeading;
	}
	
	@Override
	public List<String> getColumnList() {
		return columnList;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}