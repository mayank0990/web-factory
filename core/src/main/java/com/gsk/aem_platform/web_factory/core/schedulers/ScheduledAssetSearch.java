package com.gsk.aem_platform.web_factory.core.schedulers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.jcr.Session;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.search.Query;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * Adidas Asset Search Scheduler Job.
 * Reads Asset filenames from Excel and updates the status if Asset is found in DAM.
 */
@Component(metatype = true, label = "Adidas Asset Search Scheduler", description = "Reads Asset filenames from Excel and updates the status if Asset is found in DAM")
@Service(value = Runnable.class)
@Properties({
    @Property(name = "scheduler.expression", value = "0 */5 * ? * *", description = "Cron-job expression"),
    @Property(name = "scheduler.concurrent", boolValue=false, description = "Whether or not to schedule this task concurrently"),
})
public class ScheduledAssetSearch implements Runnable {

    private final Logger log = LoggerFactory.getLogger(getClass());
    
    @Reference
    ResourceResolverFactory resourceResolverFactory;
    
    ResourceResolver resourceResolver;
    Session session;
    
    @Reference
    private QueryBuilder queryBuilder;
    
    List<String> assetList;
    List<String> assetsNotFoundList;
    
    @Override
    public void run() {
        log.info("Scheduled Asset Search is now running, Asset List File Path='{}'", assetListFilePath);
        getAssetListFromExcel();
        getAssetSearchResult();
        
    }
    
    @Property(label = "Asset List File Path", value="/content/dam/adidas/config/assetsearch.xlsx", description = "File Path for list of asset names")
    public static final String ASSETLIST_FILEPATH = "assetlist.filepath";
    private String assetListFilePath;
    
    @Property(label = "Search Directory Path", value="/content/dam/adidas", description = "Directory Path for Asset Search")
    public static final String SEARCH_DIRECTORY_PATH = "search.directory.path";
    private String searchDirectoryPath;
    
    @Activate
    protected void activate(final Map<String, Object> config) {
        try {
	    	configure(config);
	        
	        resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
	        
        } catch (Exception e) {
        	log.error("Exception in ACTIVATE method of ScheduledAssetSearch :: " + e.getMessage());
        }
    }

    private void configure(final Map<String, Object> config) {
    	assetListFilePath = PropertiesUtil.toString(config.get(ASSETLIST_FILEPATH), "");
    	searchDirectoryPath = PropertiesUtil.toString(config.get(SEARCH_DIRECTORY_PATH), "");
    }
    
    private void getAssetListFromExcel() {
    	assetList = new ArrayList<String>();
    	
    	Resource resource = resourceResolver.getResource(assetListFilePath);
    	if((resource != null) && (resource instanceof Resource)) {
    		Resource fileResource = resourceResolver.getResource(assetListFilePath + "/jcr:content/renditions/original/jcr:content");
        	if((fileResource != null) && (fileResource instanceof Resource)){
        	    ValueMap resourceProperties = fileResource.adaptTo(ValueMap.class);
        	    InputStream fileBinary = resourceProperties.get("jcr:data", InputStream.class);
        	    
        	    try {
                    Workbook workbook = new XSSFWorkbook(fileBinary);
                    Sheet datatypeSheet = workbook.getSheetAt(0);
                    Iterator<Row> iterator = datatypeSheet.iterator();

                    while (iterator.hasNext()) {

                        Row currentRow = iterator.next();
                        Iterator<Cell> cellIterator = currentRow.iterator();

                        while (cellIterator.hasNext()) {

                            Cell currentCell = cellIterator.next();
                       
                            if (currentCell.getCellTypeEnum() == CellType.STRING) {
                                assetList.add(currentCell.getStringCellValue());
                                log.info("Asset name from Excel :: " + currentCell.getStringCellValue());
                            } 

                        }

                    }
                } catch (FileNotFoundException e) {
                	log.error("FileNotFoundException in getAssetListFromExcel method of ScheduledAssetSearch :: " + e.getMessage());
                } catch (IOException e) {
                	log.error("Exception in getAssetListFromExcel method of ScheduledAssetSearch :: " + e.getMessage());
                }
        	}
    	}
    }
    
    private void getAssetSearchResult() {
    	assetsNotFoundList = new ArrayList<String>();
    	
    	Resource resource = resourceResolver.getResource(searchDirectoryPath);
    	if((resource != null) && (resource instanceof Resource)) {
    		
    		for(String asset : assetList) {
    			boolean flag = false;
    		
    			Map<String, String> map = new HashMap<String, String>();
    			map.put("path", "/content");
    	        map.put("type", "dam:Asset");
    	        map.put("nodename", asset);
    	        
    	        Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
    	        SearchResult result = query.getResult();
    	        for (Hit hit : result.getHits()) {     
    	        	flag = true;
    	        }  
    	        
    	        if(!flag) {
    	        	assetsNotFoundList.add(asset);
    	        	log.info("Not Found :: " + asset);
    	        } else {
    	        	log.info("Found :: " + asset);
    	        }
    		}
    	}
    }
}