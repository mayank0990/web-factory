package com.gsk.aem_platform.web_factory.core.models.impl;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.gsk.aem_platform.web_factory.core.models.ListOfTeasersModel;
import com.gsk.aem_platform.web_factory.core.objects.Teaser;
import com.gsk.aem_platform.web_factory.core.objects.WCMComponent;
import com.gsk.aem_platform.web_factory.core.services.ComponentsUtilService;

/**
 * Sling Model Implementation of List Of Teasers Model
 *
 * @author Photon
 *
 */
@Model(adaptables = Resource.class, adapters = ListOfTeasersModel.class)
public class ListOfTeasersModelImpl implements ListOfTeasersModel {

	Logger logger = LoggerFactory.getLogger(ListOfTeasersModelImpl.class);

	@Inject
	ComponentsUtilService componentUtilService;
	
	@Inject @Optional
	private String customHeight; 
	
	@Inject @Optional
	private String customStyleClass;
	
	@Inject @Optional
	private String hideOnMobile;
	
	@Inject @Optional
	private String hideOnTablet;
	
	private WCMComponent wcmComponent;
	
	@Inject @Optional
	private String sectionHeading;
	
	@Inject @Optional
	private String viewType;
	
	@Inject @Optional
	@Default(values = "{}")
	private String[] teasers;

	private List<Teaser> teaserItems;
	
	@PostConstruct
	protected void init() {
		
		wcmComponent = componentUtilService.getComponentDetails("list-of-teasers", customHeight, customStyleClass, hideOnMobile, hideOnTablet);
		
		Gson gson = new Gson();
		
		teaserItems = new LinkedList<Teaser>();
		
		for (String itemString : teasers) {
			Teaser teaser = gson.fromJson(itemString, Teaser.class);
			teaserItems.add(teaser);
		}
	}
	
	@Override
	public String getViewType() {
		return viewType;
	}
	
	@Override
	public String getSectionHeading() {
		return sectionHeading;
	}
	
	@Override
	public List<Teaser> getTeaserItems() {
		return teaserItems;
	}
	
	@Override
	public WCMComponent getComponent() {
		return wcmComponent;
	}
	
}