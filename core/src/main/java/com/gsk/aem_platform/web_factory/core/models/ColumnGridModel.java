package com.gsk.aem_platform.web_factory.core.models;

import java.util.List;

/**
 * Column Grid Model
 *
 * @author Photon
 *
 */
public interface ColumnGridModel extends BaseComponentModel {
	
	public String getSectionHeading();
	
	public List<String> getColumnList();
	
}
